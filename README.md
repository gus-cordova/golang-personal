# go dev environment
Everything is relative to this checked
out repository.

./go   -> /go   (workspace)
./src  -> /src  (various sources?)

## Rebuilding the image
You should build the go compiler image so that it matches the userid you use for development; you can (re)build the image by running `./build.sh`  This will create an image `godev:latest` on your computer.

## Running commands in the container
In the image there's only the Go compiler, anything else has to happen outside, editing, listing, etc...

The `run.sh` script does several things:

* Find this repo's base directory
* Bind `$REPO/go` to `/go`
* Bind `$REPO/src` to `/src`
* If your current working directory is an offset from `$BASE` then