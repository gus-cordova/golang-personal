FROM golang:latest
COPY Dockerfile /.dockerfile
ARG USER_ID=1000
ARG GROUP_ID=1000
RUN groupadd -g ${GROUP_ID} godev \
 && useradd -u ${USER_ID} -g ${GROUP_ID} -M -d /go godev
USER ${USER_ID}:${GROUP_ID}
WORKDIR /go
