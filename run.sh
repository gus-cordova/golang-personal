#!/bin/bash

## Find relative path from base.
BASE=$(git rev-parse --show-toplevel)

if [[ $PWD == $BASE/go || $PWD == $BASE/go/* ]]; then
  WD=${PWD#$BASE}
elif [[ $PWD == $BASE/src || $PWD == $BASE/src/* ]]; then
  WD=${PWD#$BASE}
else
  WD=/src
fi

exec docker run -ti --rm \
  -u $(id -u):$(id -g) \
  -v $BASE/go:/go \
  -v $BASE/src:/src \
  -w ${WD} \
  godev "$@"
